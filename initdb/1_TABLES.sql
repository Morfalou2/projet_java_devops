create table users
(
    id bigint auto_increment,
    constraint users_pk
        primary key (id),
    first_name TEXT not null,
    last_name TEXT not null,
    age int null,
    user_name TEXT null,
    password TEXT null,
    admin_level int not null
);
create table admin_clear
(
    id_admin_level int auto_increment
        primary key,
    admin_post     text not null
);
create table products
(
    id  bigint auto_increment,
    primary key (id),
    product_name   TEXT not null,
    product_price  int  null,
    product_amount TEXT null,
    selection_amount int null
);


