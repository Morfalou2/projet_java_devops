package fr.takima.demo.DAO;

import fr.takima.demo.DAO.AdminClearance;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AdminClearDAO extends CrudRepository<AdminClearance, Long> {

}
