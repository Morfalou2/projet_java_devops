# Training Spring Boot
=====

1. Cloner le projet en utilisant la commande `git clone https://github.com/resourcepool/formation-spring-boot`

2. Importer le projet dans IntelliJ IDEA en important le fichier "pom.xml" à la racine de ce repository.

3. Exécuter votre DB mysql. Si vous avez docker, vous pouvez utiliser la commande suivante:
```
docker run --name mariadb --rm -e MYSQL_ROOT_PASSWORD=toor -e MYSQL_DATABASE=defaultdb -p 3306:3306 -v "`pwd`/initdb:/docker-entrypoint-initdb.d" mariadb
```

4. Tous les scripts sql contenus dans le dossier initdb seront exécutés automatiquement lors du premier chargement de la DB.
 
 (Forker depuis le projet https://github.com/resourcepool/formation-spring-boot.git )
 EPF Ecole d'ingenieur
 5A - MSI - P2020
 Etienne Pinet
 Stéphane Thurneyssen
 Amaury Ouin
 
 Projet de Site d'E-commerce - Java DevOps Sept/19
 
 
===== Specifique Au projet =======

1. Accés à la session admninistrateur : Username : "username" / Password :"password"

2. L'ensemble des profils utilisateurs pré-concus sont dans la BDD users, avec leurs usernames et mdp respectifs.

3. Pour ajouter un nouveau produit, il est necessaire d'ajouter son image manuellement dans le dossier "static/img" dans les ressources du projet, fichier images qui devrait être sous format jpg et nommés de la meme manière que la variable productName

========= Sources ==============================

 Les icones utilisés dans ce projets sont tirés de : https://fontawesome.com/v4.7.0/icons/ (by Aloglia®)


