package fr.takima.demo.DAO

import javax.persistence.*

/**
 *
 */
@Entity(name = "admin_clear")
data class AdminClearance (
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Id var id_admin_level: Long?,
        @Column(name = "admin_post") var adminPost: String?)
{
        constructor() : this(null, null)

}

