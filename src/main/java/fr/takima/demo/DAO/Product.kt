package fr.takima.demo.DAO

import javax.persistence.*

/**
 *
 */
@Entity(name = "products")
data class Product(
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Id var id: Long?,
        @Column(name = "product_name") var productName: String?,
        @Column(name = "product_price") var productPrice: Int?,
        @Column(name = "product_amount") var productAmount: Int?,
        @Column(name = "selection_amount") var selectionAmount: Int?) {

    constructor() : this(null, null, null, null, null)

}
