package fr.takima.demo.DAO;

import fr.takima.demo.DAO.Product;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 */
@Repository
public interface ProductDAO extends CrudRepository<Product, Long> {

}
