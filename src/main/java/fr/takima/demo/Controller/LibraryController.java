package fr.takima.demo.Controller;


import fr.takima.demo.DAO.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;



import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.util.*;


import java.util.Optional;

/**
 *
 */
@RequestMapping("/")
@Controller
public class LibraryController {

  private final UserDAO userDAO;
  private final AdminClearDAO adminClearDAO;
  private final ProductDAO productDAO;
  private Panier panier = new Panier();



  public LibraryController(UserDAO userDAO,AdminClearDAO adminClearDAO,ProductDAO productDAO) {
    this.userDAO = userDAO;
    this.adminClearDAO = adminClearDAO;
    this.productDAO = productDAO;
  }

  //////initie la bdd\\\\\\\\\____________
  @GetMapping
  public String homePage(Model m) {
    m.addAttribute("products", productDAO.findAll());
    return "index";
  }


    //////lance l'ajout d'entité user\\\\\\\\\____________
  @GetMapping("/new")
  public String addUserPage(Model m) {
    m.addAttribute("user", new User());
    return "new";
  }

    //////Retour de l'ajout d'une entité user\\\\\\\\\____________
  @PostMapping("/new")
  public RedirectView createNewUser(@ModelAttribute User user, RedirectAttributes attrs) {
    attrs.addFlashAttribute("message", "Utilisateur ajouté avec succès");
    userDAO.save(user);
    return new RedirectView("/index");
  }
  //////Retour de l'ajout d'une entité produit\\\\\\\\\____________
  @GetMapping("/new_product")
  public String addProductPage(Model m) {
    m.addAttribute("product", new Product());
    return "new_product";
  }

  //////Retour de l'ajout d'une entité produit\\\\\\\\\____________
  @PostMapping("/new_product")
  public RedirectView createNewProduct(@ModelAttribute Product product, RedirectAttributes attrs) {
    attrs.addFlashAttribute("message", "Product added");
    productDAO.save(product);
    return new RedirectView("/admin_accueil");
  }

  //////ouvrir la page admin\\\\\\\\\____________
  @GetMapping("/admin_accueil")
  public String adminPageOpen(Model m, Model m2) {
    m.addAttribute("products", productDAO.findAll());
    m2.addAttribute("users", userDAO.findAll());
    return "admin_accueil";
  }

  //////retourner à l'index\\\\\\\\\____________
  @GetMapping("index")
  public String adminPageClose(Model m) {
    m.addAttribute("products", productDAO.findAll());
    return "index";
  }


  //////Delete un user\\\\\\\\\____________
  @GetMapping("/delete/{id}")
  public RedirectView deleteUser(@ModelAttribute User user, @PathVariable long id, RedirectAttributes attrs) {
    attrs.addFlashAttribute("message", "Utilisateur supprimé avec succès");
    userDAO.deleteById(id);
    return new RedirectView("/admin_accueil");
  }
  //////Delete un produit\\\\\\\\\____________
  @GetMapping("/delete/product/{id}")
  public RedirectView deleteProduct(@ModelAttribute Product product, @PathVariable long id, RedirectAttributes attrs) {
    attrs.addFlashAttribute("message", "Product deleted");
    productDAO.deleteById(id);
    return new RedirectView("/admin_accueil");
  }


  //////Modifier un user\\\\\\\\\____________
  @GetMapping("/modif/{id}")
  public String modifUserPage(Model m, @PathVariable long id) {
    m.addAttribute("user", userDAO.findById(id));
    return "modif";
  }
  //////Retour de modification user\\\\\\\\\____________
  @PostMapping("/modif")
  public RedirectView modifUser(@ModelAttribute User user, RedirectAttributes attrs) {
    attrs.addFlashAttribute("message", "Utilisateur modifié avec succès");
    userDAO.save(user);
    return new RedirectView("/admin_accueil");
  }

  //////Modif produit\\\\\\\\\____________
  @GetMapping("/modif/product/{id}")
  public String modifProductPage(Model m, @PathVariable long id) {
    m.addAttribute("product", productDAO.findById(id));
    return "modif_product";
  }
  //////Modif produit\\\\\\\\\____________
  @PostMapping("/modif_product")
  public RedirectView modifProduct(@ModelAttribute Product product, RedirectAttributes attrs) {
    attrs.addFlashAttribute("message", "Product modified");
    productDAO.save(product);
    return new RedirectView("/admin_accueil");
  }



  /////identification\\\\\_________________
  @GetMapping("/identification")
  public String idUser(Model m) {
    m.addAttribute("objetReponse", new ObjetReponse());// creation d'un objetReponse pour contenir le mdp et username qui vont etre saisis
    return "identification";
  }

  @PostMapping("/identification")
  public RedirectView idEntryUser(@ModelAttribute ObjetReponse objetReponse, RedirectAttributes attrs) {
    Validator validator = new Validator(userDAO,adminClearDAO);// initie la class de validation (qui renvoie les niveau d'administrateur)

    User a = validator.isAuthorized(objetReponse);// Quel user possede le couple mdp/username saisi ? S'il n'existe pas, un user vide est renvoyé

    String admin_level = validator.getAdminClearance(a);// On récupère le poste d'administration (user ou admin) u
    //un user vide sera toujours d'un niveau d'administration (2) :  users simple
    if(a == new User()){// Si il n'y a pas de correspondance dans la bdd
      attrs.addFlashAttribute("message", "Utilisateur non reconnu ");
      return new RedirectView("/");//retour à l'index
    }else {//On a reconnu l'utilisateur
      String concaten = "Bonjour " + a.getFirstName();
      if(a.getFirstName()==null){
        return new RedirectView("/");
      }
      attrs.addFlashAttribute("message", concaten);
      switch (admin_level) {//selon son poste d'adminitration
        case "visiteur":
          return new RedirectView("/panier");
        case "admin":
          return new RedirectView("/admin_accueil");
        default:
          return new RedirectView("/");
      }
    }
    }




  ///Code relatif au panier\\\

  @GetMapping("panier")
  public String panierPageOpen(Model m, @CookieValue(value = "basket", defaultValue = "hello") String cookieBasket) {

    panier.cookie_product.clear();
    panier.finalList.clear();
    if (cookieBasket.equals("hello")) {
      m.addAttribute(panier);
    }
    else
    {
      String[] arr = cookieBasket.split("/");
      for (int i = 0; i < arr.length; i++) {
      panier.cookie_product.add(productDAO.findById(Long.parseLong(arr[i])));
      }
      panier.countAmount();
      m.addAttribute(panier);
    }
    return "panier";

}


  @GetMapping("/addBasket/{id}")
  public RedirectView addBasket(@ModelAttribute Product product, @PathVariable long id, RedirectAttributes attrs, HttpServletResponse response) {
    attrs.addFlashAttribute("message", "Product added to basket");
    panier.selection.add(productDAO.findById(id).get().component1());

    String list =  "";
    for (int i=0; i<panier.selection.size(); i++) {
      list += panier.selection.get(i).toString() + "/";
    }

    Cookie cookie = new Cookie("basket", list);
    cookie.setPath("/");
    response.addCookie(cookie);


    return new RedirectView("/");
  }

  @GetMapping("/deletefrombasket/{id}")
  public RedirectView deleteFromBasket(@ModelAttribute User user, @PathVariable long id, RedirectAttributes attrs, @CookieValue(value = "basket", defaultValue = "hello") String cookieBasket, HttpServletResponse response)  {
    attrs.addFlashAttribute("message", "Deleted from basket");

    panier.selection.remove(panier.selection.indexOf(id));

    Iterator<Optional<Product>> itr = panier.cookie_product.iterator();
    while (itr.hasNext()) {
      Optional<Product> prod = itr.next();
      if (prod.get().getId() == id)
      {

        panier.cookie_product.remove(itr);



        Cookie cookieRemove = new Cookie("basket", "hello");
        cookieRemove.setMaxAge(0);
        response.addCookie(cookieRemove);

        String list =  "";
        for (int i=0; i<panier.selection.size(); i++) {
          list += panier.selection.get(i).toString() + "/";
        }

        Cookie cookie = new Cookie("basket", list);
        cookie.setPath("/");
        response.addCookie(cookie);
      }
    }
    return new RedirectView("/");
  }

  }




