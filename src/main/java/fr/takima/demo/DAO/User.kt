package fr.takima.demo.DAO

import javax.persistence.*

/**
 *
 */
@Entity(name = "users")
data class User(
                @GeneratedValue(strategy = GenerationType.IDENTITY)
                @Id var id: Long?,
                @Column(name = "first_name") var firstName: String?,
                @Column(name = "last_name") var lastName: String?,
                @Column(name = "age") var age: Int?,
                @Column(name = "user_name") var userName: String?,
                @Column(name = "password") var password: String?,
                @Column(name = "admin_level") var admin_Level: Int = 2)
{
    constructor() : this(null, null, null, null,null,null,2)

}
