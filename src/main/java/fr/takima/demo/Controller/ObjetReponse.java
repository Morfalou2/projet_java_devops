package fr.takima.demo.Controller;

import java.util.Objects;

public class ObjetReponse {// objet permettant de comparer un couple mdp/username et de passer les 2 valeurs en mêmes temps
    private String username;
    private String password;

    public ObjetReponse(String a, String b){
        this.username=a;
        this.password=b;
    }

    public ObjetReponse(){

    }

    @Override
    public boolean equals(Object o) {// pour que 2 objets avec le même username et password soient égaux
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ObjetReponse that = (ObjetReponse) o;
        return Objects.equals(getUsername(), that.getUsername()) &&
                Objects.equals(getPassword(), that.getPassword());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getUsername(), getPassword());
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
