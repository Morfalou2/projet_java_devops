package fr.takima.demo.Controller;

import fr.takima.demo.Controller.ObjetReponse;
import fr.takima.demo.DAO.AdminClearDAO;
import fr.takima.demo.DAO.User;
import fr.takima.demo.DAO.UserDAO;

public class Validator {// Classe permettant de donner le niveau d'administration des users
    private UserDAO userDAO;
    private AdminClearDAO adminClearDAO;


    public Validator(UserDAO userDAO, AdminClearDAO adminclearDAO){// initialisation des bdds
        this.userDAO = userDAO;
        this.adminClearDAO = adminclearDAO;
    }

    public User isAuthorized (ObjetReponse obj){// Objet reponse sert à transmettre le mdp et l'username entré par l'user.
        String userName;
        String password;
        User userReponse = new User();//user théorique a qui on attribue le mdp et le username entrés dans la barre d'identification

        if(  (obj.getUsername().isEmpty())||(obj.getPassword().isEmpty())  ){// dans le cas où l'utilisateur essaie de valider sans avoir entré de valeur
            userName="Empty";
            password="Empty";
        }else {
            userName = obj.getUsername();//
            password = obj.getPassword();
        }


        for (User user : userDAO.findAll()){// on cherche dans la BDD un match user_entrée/user_stockés, pareil pour les mdps
            if ((user.getUserName().equals(userName))&& (user.getPassword().equals(password))){
                userReponse= user;//si il y a correspondance, on renvoie le profil complet de l'utilisateur qui veut se connecter
            }
        }
        return userReponse;// si il n'y a pas eu de match, l'user retourné est vide.
    }

    public String getAdminClearance(User user){// donne le niveau d'administration (en String) selon l'user.
        long a = user.getAdmin_Level();
        return adminClearDAO.findById(a).get().getAdminPost();
    }

}
